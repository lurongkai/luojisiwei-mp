﻿using System.Web;
using System.Web.Mvc;

namespace luojisiwei_mp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
